module.exports = function (app) {
    
    var employeeAction = require('../controller/employeeController');
    const passport = require('passport');

    app.post(
        "/employee/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            employeeAction.addNewEmployee(req.body, req.headers).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Employee has Add Successful."
            })
          );
        }
      );


      app.get(
        "/employees/:empId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            employeeAction.getEmployeeById(req.headers, req.params ).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Employee has get successfull."
            })
          );
        }
      );

      app.put(
        "/employees/:empId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            employeeAction.updateEmployeeById(req.body, req.headers, req.params).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Employee update get successfull."
            })
          );
        }
      );
     
      app.put(
        "/employees/delete/:empId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            employeeAction.deleteEmployeeById(req.headers, req.params).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Employee has Delete successfull."
            })
          );
        }
      );

      app.get(
        "/employee/:roleId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            employeeAction.getEmployeeByRole(req.headers, req.params).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Employee role has get successfull."
            })
          );
        }
      );
};
