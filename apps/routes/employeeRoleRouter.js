module.exports = function (app) {
    
    var employeeroleAction = require('../controller/employeeRoleController');
    const passport = require('passport');

    app.get('/employeeroleAction/protected', passport.authenticate('jwt', {
        session: false
    }), function (req, res) {
        res.json({
            msg: 'Congrats! You are seeing this because you are authorized'
        });
    });
}
