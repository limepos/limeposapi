module.exports = function(app) {
  var registerAction = require("../controller/registerController");
  const passport = require("passport");
  var config = require("../config/appConfig");

  app.post(
    "/register/add",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction.addNewRegister(req.body, req.headers).then(register =>
        res.status(200).json({
          data: register,
          status: true,
          message: "Register Create Successfull."
        })
      );
    }
  );

  app.get(
    "/register/:regId",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction.getRegister(req.headers, req.params).then(register =>
        res.status(200).json({
          data: register,
          status: true,
          message: "Register Get Successfull."
        })
      );
    }
  );

  app.get(
    "/registers",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction.getAllRegister(req.headers).then(register =>
        res.status(200).json({
          data: register,
          status: true,
          message: "All Register Get Successfull."
        })
      );
    }
  );

  app.put(
    "/register/update/:regId",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction
        .updateRegister(req.body, req.headers, req.params)
        .then(register =>
          res.status(200).json({
            data: register,
            status: true,
            message: "Register Update Successfull."
          })
        );
    }
  );

  app.get(
    "/registers/filter",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction
        .registerFilter(req.headers, req.query.filter)
        .then(register =>
          res.status(200).json({
            data: register,
            status: true,
            message: "Register Filter Successfull."
          })
        );
    }
  );

  app.put(
    "/registers/delete/:regId",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction
        .deleteRegister(req.headers, req.params)
        .then(register =>
          res.status(200).json({
            data: register,
            status: true,
            message: "Register Delete Successfull."
          })
        );
    }
  );

  app.post(
    "/registers/all-delete",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      registerAction
        .deleteAllRegister(req.body, req.headers )
        .then(register =>
          res.status(200).json({
            data: register,
            status: true,
            message: "All Register Delete Successfull."
          })
        );
    }
  );

  
}; //  Module End
