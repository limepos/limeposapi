module.exports = function (app) {
    
    var itemGroupAction = require('../controller/itemGroupController');
    const passport = require('passport');

    app.post(
        "/item-group/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.addNewItemGroup(req.body, req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Item Group creates Successful."
            })
          )
        }
      ); 

    

      app.get(
        "/items-groups",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.getAllItemGroup(req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "All Item Group get Successful."
            })
          );
        }
      ); 


      app.get(
        "/item-groups/:itemId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.getItemGroupById(req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Item Group get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/item-groups/:itemId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.updateItemsGroupById(req.body, req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Item Group update Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.put(
        "/item-groups/delete/:itemId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.deleteItemGroupById(req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Item Group Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.post(
        "/item-group/delete-all",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.deleteAllItemGroup(req.body, req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "All Item Group Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );


      app.get(
        "/item-group/filters",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            itemGroupAction.itemGroupFilter(req.headers, req.query.filter).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Item filter Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );


      app.post("/item-group/import",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        itemGroupAction
          .itemGroupImport(req.body, req.headers)
          .then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Item Group Import Successful."
            })
          );
      }
    );


    app.post("/item-group/export",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
        itemGroupAction
        .itemGroupExport(req.body, req.headers)
        .then(item =>
          res.status(200).json({
            data: item,
            status: true,
            message: "Item Group export Successful."
          })
        )
    }
  );  
    
};
