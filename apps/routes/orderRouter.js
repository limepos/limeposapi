module.exports = function (app) {
    
    var orderAction = require('../controller/orderController');
    const passport = require('passport');

    app.get('/orderAction/protected', passport.authenticate('jwt', {
        session: false
    }), function (req, res) {
        res.json({
            msg: 'Congrats! You are seeing this because you are authorized'
        });
    });
}
