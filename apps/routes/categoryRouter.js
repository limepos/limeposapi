module.exports = function (app) {
    
    var categoryAction = require('../controller/categoryController');
    const passport = require('passport');

    app.post(
        "/category/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            categoryAction.addNewCategory(req.body, req.headers).then(cat =>
            res.status(200).json({
              data: cat,
              status: true,
              message: "Category create Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.get(
        "/categories",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            categoryAction.getCategories(req.headers).then(cat =>
            res.status(200).json({
              data: cat,
              status: true,
              message: "categories get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.get(
        "/category/:catId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            categoryAction.getCategoryById(req.headers,req.params).then(cat =>
            res.status(200).json({
              data: cat,
              status: true,
              message: "Category get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/category/:catId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            categoryAction.categoryUpdateById( req.body, req.headers,req.params).then(cat =>
            res.status(200).json({
              data: cat,
              status: true,
              message: "Category Update Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/category/delete/:catId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            categoryAction.categoryDeleteById(req.headers,req.params).then(cat =>
            res.status(200).json({
              status: true,
              message: "Category Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.post(
        "/category/delete-all",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            categoryAction.categoryDeleteAll(req.body, req.headers).then(cat =>
            res.status(200).json({
              status: true,
              message: "All Selected Category Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.get("/categories/filters",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
         categoryAction
            .categoryFilter(req.headers, req.query.filter)
            .then(cats =>
              res.status(200).json({
                data: cats,
                status: true,
                message: "Category Filter Successful."
              })
            ).catch((e) => {
              res.status(400).json({
                 status: false,
                 message: "Some thing is wrong",
               });
         });
        }
      );

};
