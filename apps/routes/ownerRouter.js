module.exports = function(app) {
  const bcrypt = require("bcrypt");
  var Useraction = require("../controller/userController");
  const jwt = require("jsonwebtoken");
  const passport = require("passport");
  const passportJWT = require("passport-jwt");
  var config = require("../config/appConfig");
  let ExtractJwt = passportJWT.ExtractJwt;
  let JwtStrategy = passportJWT.Strategy;
  let jwtOptions = {};
  jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  jwtOptions.secretOrKey = "appsonsdevlopertsecrerkey@123";
  let strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    console.log("payload received", jwt_payload);
    let user = Useraction.getUser({
      owner_id: jwt_payload.owner_id
    });
    if (user) {
      next(null, user);
    } else {
      next(null, false);
    }
  });

  passport.use(strategy);

  app.use(passport.initialize());

  app.get("/", function(req, res) {
    console.log(config.super());
    res.json({
      message: "POS API`s Running."
    });
  });

  app.post("/register", async function(req, res, next) {
    
    let useremail = await Useraction.getUser({
      email : req.body.email
    });

    if (useremail) {
      res.status(401).json({
        message: "This email is already Register.",
        status: false
      });
    } else {
      Useraction.createUser(req.body).then(user =>
        res.status(200).json({
          message: "account created successfully",
          status: true,
          data: "user"
        })
      ).catch((e) => {
        res.status(400).json({
          message: "some thing wrong",
          status: false,
          data: e
        });
      });
    }
  });

  app.post("/login", async function(req, res, next) {
    const { email, password } = req.body;
    if (email && password) {
      let loginuser = await Useraction.getUser({
        email
      });
      if (!loginuser) {
        res.status(401).json({
          message: "No such user found",
          status: false
        });
      }
      if (
        bcrypt.compareSync(password, loginuser.password) &&
        loginuser.status == 1
      ) {
        let payload = {
          owner_id: loginuser.owner_id
        };
        let token = jwt.sign(payload, jwtOptions.secretOrKey);
        res.status(200).json({
          message: "ok",
          token: token,
          data: loginuser,
          status: true
        });
      } else {
        res.status(401).json({
          message: "Password is incorrect",
          status: false
        });
      }
    }
  });

  // Only for Superadmin of POS

  app.get(
    config.super() + "/owners",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res, next) {
      Useraction.getAllUsers(req.headers).then(user =>
        res
          .status(200)
          .json({ data: user, status: true, message: "Get All users" })
      );
    }
  );

  // Superadmin

  app.get(
    config.super() + "/owner/:ownerid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.getUsers(req.params).then(user =>
        res.status(200).json({ data: user, status: true, message: "Get users" })
      );
    }
  );

  // Superadmin

  app.put(
    config.super() + "/owner/update/:ownerid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.updateUser(req.body, req.params).then(user =>
        res
          .status(200)
          .json({ data: user, status: true, message: "Updae users" })
      );
    }
  );

  // Superadmin

  app.put(
    config.super() + "/owner/delete/:ownerid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.deleteUser(req.params).then(user => {
        res.status(200).json({
          message: "Owner Delete  Successfully",
          status: true
        });
      });
    }
  );

  // Superadmin
  app.put(
    config.super() + "/owner/disable/:ownerid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.disableUser(req.params).then(user => {
        res.status(200).json({
          message: "Owner Disable Successfully",
          status: true
        });
      });
    }
  );

  // Superadmin
  app.put(
    config.super() + "/owner/enable/:ownerid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.enableUser(req.params).then(user => {
        res.status(200).json({
          message: "Owner Enable Successfully",
          status: true
        });
      });
    }
  );

  // Superadmin
  app.get(
    config.super() + "/owners/count",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.getCount().then(user =>
        res.status(200).json({ data: user, status: true, message: "Get count" })
      );
    }
  );

  app.get(
    "/owner",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.getProfile(req.headers).then(user =>
        res.status(200).json({
          data: user,
          status: true,
          message: "Get Owner Data Successful."
        })
      );
    }
  );

  app.put(
    "/owner/update",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.updateProfile(req.body, req.headers).then(user =>
        res.status(200).json({
          data: user,
          status: true,
          message: "Profile update Successful."
        })
      );
    }
  );

  app.post(
    "/owner/change-password",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.passwordUpdate(req.body, req.headers).then(user =>
        res.status(200).json({
          data: user,
          status: true,
          message: "Password change request"
        })
      );
    }
  );

  app.post("/forgot-password", async function(req, res) {
    let user = await Useraction.getUser({
      email: req.body.email
    });
    if (!user) {
      res.status(401).json({
        message: "No such user found",
        status: false
      });
    } else {
      Useraction.forgotPasswordSendOtp(req.body).then(user =>
        res.status(200).json({ status: true, message: "Forgot Password" })
      );
    }
  });

  app.post("/forgot-password-otp-varification", async function(req, res) {
    Useraction.checkotp(req.body).then(user =>
      res.status(200).json({ status: true, message: "OTP varification." })
    );
  });

  app.post("/create-newpassword", async function(req, res) {
    Useraction.newPassword(req.body).then(user => {
      res.status(200).json({
        message: "Your password update Successfully",
        status: false
      });
    });
  });
}; // End module.
