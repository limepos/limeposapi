module.exports = function (app) {
    
    var bookingAction = require('../controller/bookingController');
    const passport = require('passport');

    app.get('/variant/protected', passport.authenticate('jwt', {
        session: false
    }), function (req, res) {
        res.json({
            msg: 'Congrats! You are seeing this because you are authorized'
        });
    });
}
