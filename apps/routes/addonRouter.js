module.exports = function (app) {
    
    var addonAction = require('../controller/addonController');
    const passport = require('passport');

     
    app.post(
        "/addon/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.addNewAddon(req.body, req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "Addon creates Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.get(
        "/addon/:addonId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.getAddonById(req.headers, req.params).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "Addon get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.get(
        "/addons",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.getAllAddon(req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "All Addon get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.put(
        "/addon/:addonId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.updateAddonById( req.body,req.headers, req.params).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "Addon updaete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/addon/delete/:addonId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.deleteAddonById(req.headers, req.params).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "Addon Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.post(
        "/addon/delete-all",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.deleteAllAddon(req.body, req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "All Addon Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );
      

      app.get(
        "/addons/filters",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonAction.addonFilter(req.headers, req.query.filter).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "Adon filter Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );


      app.post("/addon/import",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        addonAction
          .addonImport(req.body, req.headers)
          .then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "addons Import Successful."
            })
          );
      }
    );


    app.post("/addon/export",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
        addonAction
        .addonExport(req.body, req.headers)
        .then(addon =>
          res.status(200).json({
            data: addon,
            status: true,
            message: "addons export Successful."
          })
        ).catch((e) => {
          res.status(400).json({
             status: false,
             message: "Some thing is wrong",
           });
     });
    }
  );    
};