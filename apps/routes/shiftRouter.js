module.exports = function (app) {
    
    var shiftAction = require('../controller/shiftChangeController');
    const passport = require('passport');

    app.get('/variant/protected', passport.authenticate('jwt', {
        session: false
    }), function (req, res) {
        res.json({
            msg: 'Congrats! You are seeing this because you are authorized'
        });
    });
}
