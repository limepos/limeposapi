module.exports = function (app) {
    
    var taxAction = require('../controller/taxController');
    const passport = require('passport');

    app.post(
        "/tax/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.addNewTax(req.body, req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tex creates Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      ); 

      app.get(
        "/tax/:taxId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.getTaxById(req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tex Get Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );

      app.put(
        "/tax/:taxId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.updateTaxById(req.body, req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tex Update Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );

      app.get(
        "/taxes",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.getAllTaxes(req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Get All Taxes Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );


      app.put(
        "/tax/delete/:taxId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.taxDeletById(req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tax Delete Successful."
            })
          );
        }
      );

      app.post(
        "/tax/delete-all",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.allTaxDelete(req.body, req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "All Taxes has Delete Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );
      
      app.get(
        "/taxes/filter",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxAction.taxFilter(req.headers, req.query.filter).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tax filter Successful."
            })
          );
        }
      );
};
