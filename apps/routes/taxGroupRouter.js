module.exports = function (app) {
    
    var taxGroupAction = require('../controller/taxGroupController');
    const passport = require('passport');
    
    app.post(
        "/tax-group/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.addNewTaxGroup(req.body, req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tex Group creates Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      ); 

      app.get(
        "/tax-group/:taxId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.getTaxGroupById(req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tex Group Get Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );

      app.put(
        "/tax-group/:taxId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.updateTaxGroupById(req.body, req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tex Group Update Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );

      app.get(
        "/tax-groups",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.getAllTaxesGroup(req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Get All Tax Groups Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );


      app.put(
        "/tax-group/delete/:taxId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.taxGroupDeletById(req.headers, req.params).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tax group Delete Successful."
            })
          )
        }
      );

      app.post(
        "/tax-group/delete-all",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.allTaxGroupDelete(req.body, req.headers).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "All Taxes has Delete Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
          });
        }
      );
      
      app.get(
        "/taxes-group/filter",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            taxGroupAction.taxGroupFilter(req.headers, req.query.filter).then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Tax filter Successful."
            })
          );
        }
      );
};
