module.exports = function (app) {
    
    var otgAction = require('../controller/orderTicketGroupController');
    const passport = require('passport');

    app.post(
        "/orderticket/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            otgAction.addNewOrderTicket(req.body, req.headers).then(ticket =>
            res.status(200).json({
              data: ticket,
              status: true,
              message: "Order Ticket Create Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong"
              });
        });
        }
      ); 


      app.get(
        "/ordertickets",
         passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            otgAction.allOrderTickets(req.headers).then(tickets =>
            res.status(200).json({
              data: tickets,
              status: true,
              message: "All Order Ticket Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
                error : e
              });
        });
        }
      ); 

      app.get(
        "/orderticket/:orderId",
         passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            otgAction.orderTicket(req.headers,req.params).then(tickets =>
            res.status(200).json({
              data: tickets,
              status: true,
              message: "All Order Ticket Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
                error : e
              });
         });
        }
      ); 


      app.put(
           "/orderticket/delete/:orderId",
         passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            otgAction.orderTicketDelete(req.headers,req.params).then(tickets =>
            res.status(200).json({
              data: tickets,
              status: true,
              message: "All Order Ticket Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
                error : e
              });
         });
        }
      ); 

      app.put(
        "/orderticket/all-delete/",
      passport.authenticate("jwt", {
       session: false
     }),
     async function(req, res) {
         otgAction.orderTicketDeleteAll(req.body, req.headers).then(tickets =>
         res.status(200).json({
           data: tickets,
           status: true,
           message: "All Order Ticket Delete Successful."
         })
       ).catch((e) => {
          res.status(400).json({
             status: false,
             message: "Some thing is wrong",
             error : e
           });
      });
     }
   ); 

    
   
};// Module  End
