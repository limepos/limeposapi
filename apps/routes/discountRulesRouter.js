module.exports = function (app) {
    
    var discountRuleAction = require('../controller/discountRulescontroller');
    const passport = require('passport');

    app.post(
        "/discount/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            discountRuleAction.addNewDiscount(req.body, req.headers).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Discount has Add Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
        }
      );
    
      app.get(
        "/discounts",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            discountRuleAction.getAllDiscount(req.headers).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "All Discounts get Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
        }
      );

      app.get(
        "/discount/:discountId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            discountRuleAction.getDiscountById(req.headers, req.params ).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Discount gets successfull."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
        }
      );

      app.put(
        "/discount/:discountId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            discountRuleAction.updateDiscountById(req.body, req.headers, req.params).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Discount update get successfull."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
        }
      );
     
      app.put(
        "/discount/delete/:discountId",
        passport.authenticate("jwt", {
          session: false                                    
        }),
        async function(req, res) {
            discountRuleAction.deleteDiscountById(req.headers, req.params).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Discount has Delete successfull."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
        }
      );

      app.post(
        "/discount/delete-all",
        passport.authenticate("jwt", {
          session: false                                    
        }),
        async function(req, res) {
            discountRuleAction.deleteAllDiscount( req.body, req.headers).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Discount has Delete successfull."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
        }
      );

      app.post(
        "/discount/apply-coupon",
        passport.authenticate("jwt", {
          session: false                                    
        }),
        async function(req, res) {
            discountRuleAction.checkDiscountCoupon( req.body, req.headers).then(employee =>
            res.status(200).json({
              data: employee,
              status: true,
              message: "Discount checking."
            })
          );
        }
      );
};
