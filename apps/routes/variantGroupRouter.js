module.exports = function (app) {
    
    var variantGroupAction = require('../controller/variantGroupController');
    const passport = require('passport');

    app.post(
        "/variant-group/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.addNewVariantGroup(req.body, req.headers).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant Group creates Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 
  
      app.get(
        "/variant-groups",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.getVariantGroups(req.headers).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant's Group Get Successful."
            })
          )
        }
      ); 

      app.get(
        "/variant-group/:groupId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.getVariantGroupbyId(req.headers, req.params).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant Group Get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

      app.put(
        "/variant-group/:groupId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.updateVariantGroupbyId(req.body, req.headers, req.params).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant Group Update Successful."
            })
          )
        }
      );

      app.put(
        "/variant-group/delete/:groupId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.deleteVariantGroupbyId(req.headers, req.params).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant Group Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

      app.post(
        "/variant-groups/delete-all",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.deleteAllVariantGroup(req.body, req.headers, req.params).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "All Variant Group Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

      app.get(
        "/variant-groups/filters",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
          variantGroupAction.variantGroupFilter(req.headers, req.query.filter).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant Group fitler Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );


      app.post("/variant-groups/import",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        variantGroupAction
          .variantGroupImport(req.body, req.headers)
          .then(variantg =>
            res.status(200).json({
              data: variantg,
              status: true,
              message: "Variants Group Import Successful."
            })
          );
      }
    );


    app.post("/variant-groups/export",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      variantGroupAction
        .variantGroupExport(req.body, req.headers)
        .then(variant =>
          res.status(200).json({
            data: variant,
            status: true,
            message: "Variants Group export Successful."
          })
        )
    }
  );
};


