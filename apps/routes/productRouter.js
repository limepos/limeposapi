module.exports = function (app) {
    
    var productAction = require('../controller/productController');
    const passport = require('passport');
    app.post(
        "/product/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            productAction.addNewProduct(req.body, req.headers).then(product =>
            res.status(200).json({
              data: product,
              status: true,
              message: "Product has create Successful."
            })
          );
        }
      ); 

      app.get(
        "/products",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            productAction.getAllProducts(req.headers).then(prod =>
            res.status(200).json({
              data: prod,
              status: true,
              message: "Products get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.get(
        "/product/:proId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            productAction.getProductById(req.headers,req.params).then(prod =>
            res.status(200).json({
              data: prod,
              status: true,
              message: "Product get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/product/:proId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            productAction.productUpdateById( req.body, req.headers,req.params).then(prod =>
            res.status(200).json({
              data: prod,
              status: true,
              message: "Product Update Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/product/delete/:proId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            productAction.productDeleteById(req.headers,req.params).then(prod =>
            res.status(200).json({
              status: true,
              message: "Product Delete Successful."
            })
          );
        }
      ); 


      app.post(
        "/product/delete-all",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            productAction.productDeleteAll(req.body, req.headers).then(prod =>
            res.status(200).json({
              status: true,
              message: "All Selected Product Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.get("/products/filters",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
         productAction
            .productFilter(req.headers, req.query.filter)
            .then(cats =>
              res.status(200).json({
                data: cats,
                status: true,
                message: "Product Filter Successful."
              })
            ).catch((e) => {
              res.status(400).json({
                 status: false,
                 message: "Some thing is wrong",
               });
         });
        }
      );

      app.post("/products/import",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        productAction
          .productImport(req.body, req.headers)
          .then(item =>
            res.status(200).json({
              data: item,
              status: true,
              message: "Product Import Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
      }
    );


    app.post("/products/export",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
        productAction
        .productExport(req.body, req.headers)
        .then(item =>
          res.status(200).json({
            data: item,
            status: true,
            message: "Product export Successful."
          })
        ).catch((e) => {
          res.status(400).json({
             status: false,
             message: "Some thing is wrong",
           });
     });
    }
  ); 
  
  
  app.post("/products/queckadd",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
        productAction
        .productQueckAdd(req.body, req.headers)
        .then(item =>
          res.status(200).json({
            data: item,
            status: true,
            message: "Product export Successful."
          })
        ).catch((e) => {
          res.status(400).json({
             status: false,
             message: "Some thing is wrong",
           });
     });
    }
  );

};
 