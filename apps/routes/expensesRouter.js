module.exports = function (app) {
    
    var expensesAction = require('../controller/expensesController');
    const passport = require('passport');

    app.get('/expensesAction/protected', passport.authenticate('jwt', {
        session: false
    }), function (req, res) {
        res.json({
            msg: 'Congrats! You are seeing this because you are authorized'
        });
    });
}
