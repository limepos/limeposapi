module.exports = function (app) {

    require('./ownerRouter')(app);
    require('./shopRouter')(app);
    require('./registerRouter')(app);
    require('./orderTicketRouter')(app);
    require('./variantRouter')(app);
    require('./variantGroupRouter')(app);
    require('./categoryRouter')(app);
    require('./addonRouter')(app);
    require('./addnGroupRouter')(app);
    require('./itemGroupRouter')(app);
    require('./taxRouter')(app);
    require('./taxGroupRouter')(app);
    require('./productRouter')(app);
    require('./employeeRouter')(app);
    require('./employeeRoleRouter')(app);
    require('./discountRulesRouter')(app);
    require('./receiptRouter')(app);
    require('./expensesRouter')(app);
    require('./bookingRouter')(app);
    require('./shiftRouter')(app);
    require('./customersRouter')(app);
    

    // var Useraction = require('../controller/userController');
    // var ShopAction = require('../controller/shopController');
    // var registerAction = require('../controller/registerController');
    // var orderTiketGroupAction = require('../controller/orderTicketGroupController');
    // var categoryAction = require('../controller/categoryController');
    // var variantAction = require('../controller/variantController');
    // var variantGroupAction = require('../controller/variantGroupController');
    // var addonAction = require('../controller/addonController');
    // var addonGroupAction = require('../controller/addonGroupController');
    // var itemGroupAction = require('../controller/itemGroupController');
    // var taxAction = require('../controller/taxController');
    // var taxGroupAction = require('../controller/taxGroupController');
    // var productAction = require('../controller/productController');
    // var employeeAction = require('../controller/employeeController');
    // var employeeroleAction = require('../controller/employeeRoleController');
    // var discountRuleAction = require('../controller/discountRulescontroller');
    // var receiptAction = require('../controller/receiptController');
    // var expensesAction = require('../controller/expensesController');
    // var customerAction = require('../controller/customerController'); 
    // var orderAction = require('../controller/orderController');  
    // var bookingAction = require('../controller/bookingController');  
    // var shiftAction = require('../controller/shiftChangeController');  
}
