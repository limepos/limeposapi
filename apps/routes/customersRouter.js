module.exports = function (app) {
    
    var customerAction = require('../controller/customerController');
    const passport = require('passport');

    app.get('/customerAction/protected', passport.authenticate('jwt', {
        session: false
    }), function (req, res) {
        res.json({
            msg: 'Congrats! You are seeing this because you are authorized'
        });
    });
}
