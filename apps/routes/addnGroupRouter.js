module.exports = function (app) {
    
    var addonGroupAction = require('../controller/addonGroupController');
    const passport = require('passport');
    
    app.post(
        "/addon-group/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.addNewAddonsGroup(req.body, req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "AddonsGroup creates Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.get(
        "/addon-group/:addonId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.getAddonsGroupById(req.headers, req.params).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "AddonsGroup get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.get(
        "/addon-groups",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.getAllAddonsGroup(req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "All AddonsGroup get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 


      app.put(
        "/addon-group/:addonId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.updateAddonsGroupById( req.body,req.headers, req.params).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "AddonsGroup updaete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.put(
        "/addon-group/delete/:addonId",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.deleteAddonsGroupById(req.headers, req.params).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "AddonsGroup Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.post(
        "/addon-group/delete-all",
          passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.deleteAllAddonsGroup(req.body, req.headers).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "All AddonsGroup Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );
      

      app.get(
        "/addon-groups/filters",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            addonGroupAction.addonGroupFilter(req.headers, req.query.filter).then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "Adon filter Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );


      app.post("/addon-group/import",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        addonGroupAction
          .addonaGroupImport(req.body, req.headers)
          .then(addon =>
            res.status(200).json({
              data: addon,
              status: true,
              message: "addons Import Successful."
            })
          );
      }
    );


    app.post("/addon-group/export",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
        addonGroupAction
        .addonGroupExport(req.body, req.headers)
        .then(addon =>
          res.status(200).json({
            data: addon,
            status: true,
            message: "addons export Successful."
          })
        ).catch((e) => {
          res.status(400).json({
             status: false,
             message: "Some thing is wrong",
           });
     });
    }
  );  
    
};
 