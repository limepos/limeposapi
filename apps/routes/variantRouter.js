module.exports = function (app) {
    
    var variantAction = require('../controller/variantController');
    const passport = require('passport');

    app.post(
        "/variant/add",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            variantAction.addNewVariant(req.body, req.headers).then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant creates Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      ); 

      app.get(
        "/variants",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            variantAction.getAllVarinat(req.headers).then(variants =>
            res.status(200).json({
              data: variants,
              status: true,
              message: "Variants get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

      app.get(
        "/variant/:varinatId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            variantAction.getVarinatById(req.headers,req.params).then(variants =>
            res.status(200).json({
              data: variants,
              status: true,
              message: "Variant get Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

      app.put(
        "/variant/:varinatId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            variantAction.updateVarinatById(req.body, req.headers, req.params).then(variants =>
            res.status(200).json({
              data: variants,
              status: true,
              message: "Variant update Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

      app.put(
        "/variant/delete/:varinatId",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            variantAction.deleteVarinatById(req.headers, req.params).then(variants =>
            res.status(200).json({
              data: variants,
              status: true,
              message: "Variant Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );


      app.post(
        "/variant/delete-all",
        passport.authenticate("jwt", {
          session: false
        }),
        async function(req, res) {
            variantAction.deleteAllVarinatById(req.body, req.headers).then(variants =>
            res.status(200).json({
              data: variants,
              status: true,
              message: "Variant Delete Successful."
            })
          ).catch((e) => {
             res.status(400).json({
                status: false,
                message: "Some thing is wrong",
              });
        });
        }
      );

   
      app.get("/variants/filters",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        variantAction
          .variantFilter(req.headers, req.query.filter)
          .then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variant Filter Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
      }
    );


    app.post("/variants/import",
      passport.authenticate("jwt", {
        session: false
      }),
      async function(req, res) {
        variantAction
          .variantImport(req.body, req.headers)
          .then(variant =>
            res.status(200).json({
              data: variant,
              status: true,
              message: "Variants Import Successful."
            })
          ).catch((e) => {
            res.status(400).json({
               status: false,
               message: "Some thing is wrong",
             });
       });
      }
    );


    app.post("/variants/export",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      variantAction
        .variantExport(req.body, req.headers)
        .then(variant =>
          res.status(200).json({
            data: variant,
            status: true,
            message: "Variants export Successful."
          })
        ).catch((e) => {
          res.status(400).json({
             status: false,
             message: "Some thing is wrong",
           });
     });
    }
  );

};
