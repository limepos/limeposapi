module.exports = function(app) {
  var ShopAction = require("../controller/shopController");
  const passport = require("passport");
  var config = require('../config/appConfig');
  app.post(
    "/new-shop/",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      ShopAction.addNewShop(req.body).then(shop =>
        res.status(200).json({
          data: shop,
          status: true,
          message: "Shop Create Successfull."
        })
      );
    }
  );

  app.put(
    "/shop/update",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      ShopAction.shopUpdate(req.body, req.headers).then(shop =>
        res.status(200).json({
          data: shop,
          status: true,
          message: "Shop Update Successfull."
        })
      );
    }
  );
  
  //super admin 
  app.get(
    config.super() + "/shops/",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      ShopAction.getAllShops().then(shops =>
        res.status(200).json({
          data: shops,
          status: true,
          message: "Get all Shops Successful."
        })
      );
    }
  );

  app.get(
    "/shop",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      ShopAction.getShop(req.headers).then(shop =>
        res.status(200).json({
          data: shop,
          status: true,
          message: "Shop Get Successful."
        })
      );
    }
  );
}; // Module end
