var Addon  = require('../models/addonModel');
var fs = require('fs');
var json2xls = require('json2xls');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const config = require("../config/emailConfig.js");

exports.addNewAddon = async (addondata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await Addon.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const variatId = num.toString().padStart(5, "0");
        addondata.addon_id = owner_id + "" + variatId;
        addondata.addon_status = 1;
        addondata.owner_id = owner_id;
        addondata.sequelnumber = num;
        return await Addon.create(addondata);
      });
    });
  };

  exports.getAddonById = async (author,params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await Addon.findOne({ where : {
            owner_id: owner_id,
            addon_id:  params.addonId,
            addon_status : 1
        }
       });
    }); 
  };

  exports.getAllAddon = async (author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await Addon.findAll({ where : {
            owner_id: owner_id,
            addon_status : 1
        },
        order: [
            ['addon_sort_order', 'ASC']
          ] 
       });
    }); 
  };


  exports.updateAddonById = async (data, author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await Addon.update(data ,{ where : {
            owner_id: owner_id,
            addon_id:  params.addonId,
            addon_status : 1
        }
       });
    }); 
  };

  exports.deleteAddonById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await Addon.update({addon_status : 0},{ where : {
            owner_id: owner_id,
            addon_id:  params.addonId,
            addon_status : 1
        }
       });
    }); 
  };

  exports.deleteAllAddon = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        let ids = data.ids.split(",");
        // let ids = ['00010001', '00010002'];

          return await  Addon.update(
               { addon_status : 0 },
                 { where : 
                     {   addon_id : ids, 
                         owner_id:owner_id 
                    }
                });
         
       }); 
  };

  
  exports.addonFilter = async (author, query) => {
    var obj = JSON.parse(query);
   // console.log("filter ==> ", obj);
    return await owner.getOwnerId(author).then(async owner_id => {
        obj.owner_id = owner_id;
        obj.addon_status = 1;
          return await Addon.findAll({
            where: obj
          });
    });
  };

  exports.addonImport = async (addondata, author) => {

      return await owner.getOwnerId(author).then(async owner_id => {
    
     console.log("json ==> ", addondata.data);

     let data = JSON.parse(addondata.data);
   
  
      for ( var key in data.data) {
        await Addon.findAll({
          attributes: [
            [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
          ],
          where : {
            owner_id : owner_id
           }
        }).then(async (id) => {
          var count = JSON.stringify(id);
          var pp = JSON.parse(count);
          
          var num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
          var addonId = num.toString().padStart(5, "0");
          data.data[key].addon_id = owner_id + "" + addonId;   //Combination of  Owner id and Generated category id 
          data.data[key].addon_status = 1;
          data.data[key].owner_id = owner_id;
          data.data[key].sequelnumber = num;
           await Addon.create(data.data[key]);
        });
       }
      });
    };
  
      
    exports.addonExport = async (userdata, author) => {
  
     //   console.log("jhasdkjf",userdata.email);
  
      return await owner.getOwnerId(author).then(async owner_id => {
        
        return await Addon.findAll({ 
             owner_id: owner_id,
             addon_status : 1
         }).then( async data => {
  
          var count = JSON.stringify(data);
          var p = JSON.parse(count);
          
          // console.log("variants", p);
           var xls = json2xls(p);
           //console.log(xls);
          // fs.writeFileSync('exportsdata/variant.xlsx', xls, 'binary');
  
           const transporter = config.transporter();
    
          const mailOptions = {
            from: config.fromEmail(),
            to: userdata.email,
            subject: "Lime POS Addon Data",
            text:
              "Hi please find attachment for Addon export data.",
              attachments: [
                {   // utf-8 string as an attachment
                    filename: 'addon'+Date.now()+'.xlsx',
                    content: new Buffer( xls, 'binary')
                }
              ]
          };
    
          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              console.log(error);
            } else {
              return "Addon Export Sucessfull";
            }
          });
  
        });
      });
    };
  
  
  
  