var tax  = require('../models/taxModel');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");

exports.addNewTax = async (taxdata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await tax.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const taxId = num.toString().padStart(5, "0");
        taxdata.tax_id = owner_id + "" + taxId;
        taxdata.tax_status = 1;
        taxdata.owner_id = owner_id;
        taxdata.sequelnumber = num;
        return await tax.create(taxdata);
      });
    });
  };

  exports.getTaxById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await tax.findOne(
            { 
             where: {
              owner_id:owner_id,
              tax_status : 1,
              tax_id : params.taxId
              }
           },   
        );
    });
  };

  exports.updateTaxById = async (data, author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await tax.update(data,
            { 
             where: {
               owner_id:owner_id,
               tax_id : params.taxId
              }
           },   
        );
    });
  };



  exports.getAllTaxes = async (author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await tax.findAll(
            { 
             where: {
              owner_id: owner_id,
              tax_status : 1
              }
           },   
        );
    });
  };


  exports.taxDeletById = async ( author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await tax.update({tax_status: 0},
            { 
             where: {
               owner_id:owner_id,
               tax_id :  params.taxId
              }
           },   
        );
    });
  };

     
  exports.allTaxDelete = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
    console.log(data);
    let ids = data.ids.split(",");
     // let ids = ['00010001', '00010002'];
       return await  tax.update({ tax_status : 0 },{ where : { tax_id : ids, owner_id: owner_id }} );
      });
    
  };

  exports.taxFilter = async (author, query) => {
    var obj = JSON.parse(query);
    console.log("filter == > ", obj);
    return await owner.getOwnerId(author).then(async owner_id => {
        obj.owner_id = owner_id;
        obj.tax_status = 1;
          return await tax.findAll({
            where: obj
          });
    });
  };