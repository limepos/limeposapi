var addonGroup  = require('../models/addonGroupModel');
var addonGroupItem  = require('../models/addonGroupItem');
var fs = require('fs');
var json2xls = require('json2xls');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const config = require("../config/emailConfig.js");

exports.addNewAddonsGroup = async (addondata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await addonGroup.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const variatId = num.toString().padStart(5, "0");
        addondata.addon_group_id = owner_id + "" + variatId;
        addondata.adg_status = 1;
        addondata.owner_id = owner_id;
        addondata.sequelnumber = num;
        return await addonGroup.create(addondata).then((addonGroups)=>{
             
          console.log("variantIds", addonGroup.owner_id);
           
          var ids = addondata.addon_ids.split(",");

          console.log(ids);

          var i;
          let addonItem = { };
          addonItem.owner_id =  addonGroups.owner_id;
          addonItem.addon_group_id = addonGroups.addon_group_id;
          addonItem.item_status = 1;
          for (i = 0; i < ids.length; i++) {
            addonItem.addon_groups_item_id = Date.now()+'-'+ids[i];
            addonItem.addon_id = ids[i];
           // console.log("varinatItem", varinatItem);
              addonGroupItem.create(addonItem);
          }

       });
      });
    });
  };

  exports.getAddonsGroupById = async (author,params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      addonGroup.hasMany(addonGroupItem, {foreignKey: 'addon_group_id'});
        return await addonGroup.findOne({ where : {
            owner_id: owner_id,
            addon_group_id:  params.addonId,
            adg_status : 1
        },
        include: [
          { model: addonGroupItem, attributes: ['addon_id'] }
      ] 
       });
    }); 
  };

  exports.getAllAddonsGroup = async (author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      addonGroup.hasMany(addonGroupItem, {foreignKey: 'addon_group_id'});
        return await addonGroup.findAll({ where : {
            owner_id: owner_id,
            adg_status : 1
        },
        order: [
            ['adg_sort_order', 'ASC']
          ],
          include: [
            { model: addonGroupItem, attributes: ['addon_id'] }
        ] 
       });
    }); 
  };


  exports.updateAddonsGroupById = async (data, author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await addonGroup.update(data ,{ where : {
            owner_id: owner_id,
            addon_group_id:  params.addonId,
            adg_status : 1
        }
       }).then((addonGroup)=>{

        console.log("==>", data); 
        console.log("==>", params.addonId);
        
     //   let variant = variantGroup;
        
     addonGroupItem.destroy({ where : {
          addon_group_id:  params.addonId
       }
      }).then(() => {
       console.log("variantIds", data );
       var ids = data.addon_ids.split(",");
       console.log(ids);
       var i;
       let addonItem = { };
       addonItem.owner_id =  owner_id;
       addonItem.addon_group_id = params.addonId;
       addonItem.item_status = 1;
       for (i = 0; i < ids.length; i++) {
        addonItem.addon_groups_item_id = Date.now()+'-'+ids[i];
        addonItem.addon_id = ids[i];
        // console.log("varinatItem", varinatItem);
           addonGroupItem.create(addonItem);
       }
     });
       
     });
    }); 
  };

  exports.deleteAddonsGroupById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await addonGroup.update({adg_status : 0},{ where : {
            owner_id: owner_id,
            addon_group_id:  params.addonId,
            adg_status : 1
        }
       });
    }); 
  };

  exports.deleteAllAddonsGroup = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        let ids = data.ids.split(",");
        // let ids = ['00010001', '00010002'];

          return await  addonGroup.update(
               { adg_status : 0 },
                 { where : 
                     {   addon_group_id : ids, 
                         owner_id:owner_id 
                    }
                });
         
       }); 
  };

  
  exports.addonGroupFilter = async (author, query) => {
    var obj = JSON.parse(query);
   // console.log("filter ==> ", obj);
    return await owner.getOwnerId(author).then(async owner_id => {
        obj.owner_id = owner_id;
        obj.adg_status = 1;
          return await addonGroup.findAll({
            where: obj
          });
    });
  };

  exports.addonaGroupImport = async (addondata, author) => {

      return await owner.getOwnerId(author).then(async owner_id => {
    
     console.log("json ==> ", addondata.data);

     let data = JSON.parse(addondata.data);
   
  
      for ( var key in data.data) {
        await addonGroup.findAll({
          attributes: [
            [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
          ],
          where : {
            owner_id : owner_id
           }
        }).then(async (id) => {
          var count = JSON.stringify(id);
          var pp = JSON.parse(count);
          
          var num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
          var addonId = num.toString().padStart(5, "0");
          data.data[key].addon_group_id = owner_id + "" + addonId;   //Combination of  Owner id and Generated category id 
          data.data[key].adg_status = 1;
          data.data[key].owner_id = owner_id;
          data.data[key].sequelnumber = num;
           await addonGroup.create(data.data[key]).then((addonGroups)=>{
             
            console.log("variantIds", addonGroup.owner_id);
             
            var ids = data.data[key].addon_ids.split(",");
  
            console.log(ids);
  
            var i;
            let addonItem = { };
            addonItem.owner_id =  addonGroups.owner_id;
            addonItem.addon_group_id = addonGroups.addon_group_id;
            addonItem.item_status = 1;
            for (i = 0; i < ids.length; i++) {
              addonItem.addon_groups_item_id = Date.now()+'-'+ids[i];
              addonItem.addon_id = ids[i];
             // console.log("varinatItem", varinatItem);
                addonGroupItem.create(addonItem);
            }
  
         });
        });
       }
      });
    };
  
      
    exports.addonGroupExport = async (userdata, author) => {
  
     //   console.log("jhasdkjf",userdata.email);
  
      return await owner.getOwnerId(author).then(async owner_id => {
        addonGroup.hasMany(addonGroupItem, {foreignKey: 'addon_group_id'});
        return await addonGroup.findAll({ where:{ 
             owner_id: owner_id,
             adg_status : 1
         },
         include: {
           model: addonGroupItem, attributes: ['addon_id'] 
         }
        }).then( async data => {
  
          var count = JSON.stringify(data);
          var p = JSON.parse(count);
          
          p.forEach(e => {
             console.log(">>>", e);
        
             var  variant_id = [];
            

             e.res_product_addon_group_items.forEach(id => {
              console.log(">>>", id.addon_id);
              variant_id.push(id.addon_id);
          });
          e.addon_ids = variant_id;
        });

          // console.log("variants", p);
           var xls = json2xls(p,{
            fields: ['addon_group_name','min_selectable','max_selectable','addon_ids','adg_sort_order']
        });
           //console.log(xls);
          // fs.writeFileSync('exportsdata/variant.xlsx', xls, 'binary');
  
           const transporter = config.transporter();
    
          const mailOptions = {
            from: config.fromEmail(),
            to: userdata.email,
            subject: "Lime POS Addon Group Data",
            text:
              "Hi please find attachment for Addon Group export data.",
              attachments: [
                {   // utf-8 string as an attachment
                    filename: 'addon'+Date.now()+'.xlsx',
                    content: new Buffer( xls, 'binary')
                }
              ]
          };
    
          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              console.log(error);
            } else {
              return "Addon Group Export Sucessfull";
            }
          });
  
        });
      });
    };