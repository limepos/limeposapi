var Category  = require('../models/categoryModel');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
exports.addNewCategory = async (catdata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
      return await Category.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const catId = num.toString().padStart(5, "0");
        catdata.category_id = owner_id + "" + catId;   //Combination of  Owner id and Generated category id 
        catdata.category_status = 1;
        catdata.owner_id = owner_id;
        catdata.sequelnumber = num;
        return await Category.create(catdata);
      });
    });
  };

  exports.getCategories = async (author) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await Category.findAll(
            { 
             where: {
              owner_id:owner_id,
              category_status : 1
              },
              order: [
                ['cat_sort_order', 'ASC']
              ],
           },
        
        );
     
    });
  };

  exports.getCategoryById = async (author,params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await Category.findOne(
            { 
             where: {
              owner_id:owner_id,
              category_id : params.catId,
              category_status : 1
              }
            }
        );
    });
  };

  exports.categoryUpdateById = async (catdata, author, params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await Category.update(catdata,
            { 
             where: {
              owner_id:owner_id,
              category_id : params.catId
              
              }
            }
        );
    });
  };

  exports.categoryDeleteById = async (author, params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await Category.update({category_status : 0 },
            { 
             where: {
              owner_id:owner_id,
              category_id : params.catId
              
              }
            }
        );
    });
  };
  
  
  exports.categoryDeleteAll = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
    console.log(data);
    let ids = data.ids.split(",");
     // let ids = ['00010001', '00010002'];
       return await  Category.update({ category_status : 0 },{ where : { category_id : ids, owner_id:owner_id }} );
      });
    
  };
  

  exports.categoryFilter = async (author, query) => {
    var obj = JSON.parse(query);
    console.log("filter == > ", obj);
    return await owner.getOwnerId(author).then(async owner_id => {
        obj.owner_id = owner_id;
        obj.category_status = 1;
          return await Category.findAll({
            where: obj
          });
    });
  };
  
   