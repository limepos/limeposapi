const jwtDecode = require("jwt-decode");
exports.getOwnerId = async headers => {
  var token = headers.authorization.split(" ");
  var decoded = jwtDecode(token[1]);
  return decoded.owner_id;
};
