var Register = require("../models/registerModel");
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");

exports.addNewRegister = async (registerdata, author) => {

  return await owner.getOwnerId(author).then(async owner_id => {
  
    return await Register.findAll({
      attributes: [
        [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
      ],
      where : {
        owner_id : owner_id
       }
    }).then(async data => {
      let count = JSON.stringify(data);
      let pp = JSON.parse(count);
      let dd = 0;
       let num;
         if(pp[0].count) {
             dd = pp[0].count;
             num = parseInt(dd) + 1;
           
          } else {
            dd =  1;
            num = parseInt(dd);
         }
      const registerId = num.toString().padStart(4, "0");
      registerdata.registers_id = owner_id + "" + registerId;
      registerdata.register_status = 1;
      registerdata.owner_id = owner_id;
      registerdata.sequelnumber = num;
      return await Register.create(registerdata);
    });
  });
};

exports.getRegister = async (author, params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await Register.findOne({
      where: {
        owner_id: owner_id,
        registers_id: params.regId,
        register_status : 1
      }
    });
  });
};

exports.getAllRegister = async author => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await Register.findAll({
      where: {
        owner_id: owner_id,
        register_status : 1 
      }
    });
  });
};

exports.updateRegister = async (data, author, params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await Register.update(data, {
      where: {
        owner_id: owner_id,
        registers_id: params.regId
      }
    });
  });
};

exports.registerFilter = async (author, query) => {
  var obj = JSON.parse(query);
  return await owner.getOwnerId(author).then(async owner_id => {
      obj.owner_id = owner_id;
      obj.register_status = 1 ;
        return await Register.findAll({
          where: obj
        });
  });
};

exports.deleteRegister = async (author, params) => {
   return await owner.getOwnerId(author).then(async owner_id => {
        return await Register.update({register_status : 0 },{
          where: {
              owner_id : owner_id,
               registers_id :  params.regId
          }
        });
  });
};

exports.deleteAllRegister = async (data, author ) => {
  return await owner.getOwnerId(author).then(async owner_id => {
  console.log(data);
  let ids = data.ids.split(",");
   // let ids = ['00010001', '00010002'];
     return await  Register.update({ register_status : 0 },{ where : { registers_id : ids }} );
    });
  
};
