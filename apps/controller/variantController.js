var Variant  = require('../models/variantModel');
var fs = require('fs');
var json2xls = require('json2xls');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const config = require("../config/emailConfig.js");


exports.addNewVariant = async (varinatdata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
      return await Variant.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const variatId = num.toString().padStart(5, "0");
        varinatdata.variant_id = owner_id + "" + variatId;   //Combination of  Owner id and Generated category id 
        varinatdata.variant_status = 1;
        varinatdata.owner_id = owner_id;
        varinatdata.sequelnumber = num;
        return await Variant.create(varinatdata);
      });
    });
  };

  exports.getAllVarinat = async (author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
    return await Variant.findAll({ where : {
       variant_status : 1,
       owner_id : owner_id
    }
  });
 }); 
};

exports.getVarinatById = async (author,params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
  return await Variant.findOne({ where : {
     variant_status : 1,
     variant_id : params.varinatId,
     owner_id : owner_id
  }
});
}); 
};

exports.updateVarinatById = async ( varinatdata, author,params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
  return await Variant.update(varinatdata, { where : {
     variant_status : 1,
     variant_id : params.varinatId,
     owner_id : owner_id
  }
});
}); 
};

exports.deleteVarinatById = async (author,params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
  return await Variant.update({ variant_status : 0 }, { where : {
     variant_id : params.varinatId,
     owner_id : owner_id
  }
});
}); 
};

exports.deleteAllVarinatById = async (data, author,params) => {

  return await owner.getOwnerId(author).then(async owner_id => {
    console.log(data);
    let ids = data.ids.split(",");
     // let ids = ['00010001', '00010002'];
       return await  Variant.update({ variant_status : 0 },{ where : { variant_id : ids, owner_id:owner_id }} );
      });
    
  };

  exports.variantFilter = async (author, query) => {
    var obj = JSON.parse(query);
    console.log("filter ==> ", obj);
    return await owner.getOwnerId(author).then(async owner_id => {
        obj.owner_id = owner_id;
        obj.variant_status = 1;
          return await Variant.findAll({
            where: obj
          });
    });
  };


  exports.variantImport = async (author, query) => {
    
    User.bulkCreate().then(() => { // Notice: There are no arguments here, as of right now you'll have to...
      return User.findAll();
    }).then(users => {
      console.log(users); // ... in order to get the array of user objects
    });
  };

  

  exports.variantImport = async (varinatdata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => {

   let data = JSON.parse(varinatdata.data);
    //console.log("json ==> ", data);

    for ( var key in data.data) {
      await Variant.findAll({
        attributes: [
          [Sequelize.fn("COUNT", Sequelize.col("variant_id")), "count"]
        ]
      }).then(async (id) => {
        var count = JSON.stringify(id);
        var p = JSON.parse(count);
        var num = p[0].count + 1;
        var variatId = num.toString().padStart(5, "0");
        data.data[key].variant_id = owner_id + "" + variatId;   //Combination of  Owner id and Generated category id 
        data.data[key].variant_status = 1;
        data.data[key].owner_id = owner_id;
         await Variant.create(data.data[key]);
      });
     }
    });
  };

  exports.variantImport = async (varinatdata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => {

   let data = JSON.parse(varinatdata.data);
    //console.log("json ==> ", data);

    for ( var key in data.data) {
      await Variant.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async (id) => {
        var count = JSON.stringify(id);
        var pp = JSON.parse(count);
          
          var num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        var variatId = num.toString().padStart(5, "0");
        data.data[key].variant_id = owner_id + "" + variatId;   //Combination of  Owner id and Generated category id 
        data.data[key].variant_status = 1;
        data.data[key].owner_id = owner_id;
        data.data[key].sequelnumber = num;
         await Variant.create(data.data[key]);
      });
     }
    });
  };

    
  exports.variantExport = async (userdata, author) => {

   //   console.log("jhasdkjf",userdata.email);

    return await owner.getOwnerId(author).then(async owner_id => {
      
      return await Variant.findAll({
        variant_status : 1,
        owner_id : owner_id
      }).then( async data => {

        var count = JSON.stringify(data);
        var p = JSON.parse(count);
        
        // console.log("variants", p);
         var xls = json2xls(p);
         //console.log(xls);
        // fs.writeFileSync('exportsdata/variant.xlsx', xls, 'binary');

         const transporter = config.transporter();
  
        const mailOptions = {
          from: config.fromEmail(),
          to: userdata.email,
          subject: "Lime POS Variant Data",
          text:
            "Hi please find attachment for variant export data.",
            attachments: [
              {   // utf-8 string as an attachment
                  filename: 'variant'+Date.now()+'.xlsx',
                  content: new Buffer( xls, 'binary')
              }
            ]
        };
  
        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            return "Varint Export Sucessfull";
          }
        });

      });
    });
  };
  