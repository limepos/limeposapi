var discountRule  = require('../models/discountRulesModel');
var discountItem  = require('../models/discountRulesItem');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");


exports.addNewDiscount = async (discountdata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await discountRule
        .findAll({
          attributes: [
            [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
          ],
          where: {
            owner_id: owner_id
          }
        })
        .then(async data => {
          let count = JSON.stringify(data); 
          let pp = JSON.parse(count);
          let dd = 0;
          let num;
          if (pp[0].count) {
            dd = pp[0].count;
            num = parseInt(dd) + 1;
          } else {
            dd = 1;
            num = parseInt(dd);
          }
          const discoutnId = num.toString().padStart(5, "0");
          discountdata.discount_rule_id = owner_id + "" + discoutnId;
          discountdata.discount_status = 1;
          discountdata.owner_id = owner_id;
          discountdata.sequelnumber = num;
          return await discountRule.create(discountdata).then(itemdata => {
            var ids = discountdata.registers_ids.split(",");
             console.log("ids", ids);
            var i;
            let gItem = {};
            gItem.owner_id = itemdata.owner_id;
            gItem.discount_rule_id = itemdata.discount_rule_id;
            gItem.item_status = 1;
            for (i = 0; i < ids.length; i++) {
              gItem.discount_rules_item_id = Date.now() + "-" + ids[i];
              gItem.registers_id = ids[i];
              discountItem.create(gItem);
            }
          });
        });
    });
  };

  exports.getAllDiscount = async author => {
    return await owner.getOwnerId(author).then(async owner_id => {
        discountRule.hasMany(discountItem, { foreignKey: "discount_rule_id" });
      return await discountRule.findAll({
        where: {
          owner_id: owner_id,
          discount_status: 1
        },
        include: [{ model: discountItem, attributes: ["registers_id"] }]
      });
    });
  };


  exports.getDiscountById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        discountRule.hasMany(discountItem, { foreignKey: "discount_rule_id" });
      return await discountRule.findOne({
        where: {
          owner_id: owner_id,
          discount_status: 1,
          discount_rule_id: params.discountId
        },
        include: [{ model: discountItem, attributes: ["registers_id"] }]
      });
    });
  };

  exports.updateDiscountById = async (data, author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await discountRule
        .update(data, {
          where: {
            owner_id: owner_id,
            discount_rule_id: params.discountId,
            discount_status: 1
          }
        })
        .then(() => {
            discountItem
            .destroy({
              where: {
                discount_rule_id: params.discountId
              }
            })
            .then(() => {
              console.log("variantIds", data);
              var ids = data.registers_ids.split(",");
              console.log(ids);
              var i;
              let gItem = {};
              gItem.owner_id = owner_id;
              gItem.discount_rule_id = params.discountId;
              gItem.item_status = 1;
              for (i = 0; i < ids.length; i++) {
                gItem.discount_rules_item_id = Date.now() + "-" + ids[i];
                gItem.registers_id = ids[i];
                discountItem.create(gItem);
              }
            });
        });
    });
  };

  exports.deleteDiscountById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await discountRule.update(
        { discount_status: 0 },
        {
          where: {
            owner_id: owner_id,
            discount_rule_id: params.discountId,
            discount_status: 1
          }
        }
      );
    });
  };
  
  exports.deleteAllDiscount = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      let ids = data.ids.split(",");
  
      return await discountRule.update(
        { discount_status: 0 },
        {
          where: {
            discount_rule_id: ids,
            owner_id: owner_id
          }
        }
      );
    });
  };
  
  exports.discountFilter = async (author, query) => {
    var obj = JSON.parse(query);
  
    return await owner.getOwnerId(author).then(async owner_id => {
      obj.owner_id = owner_id;
      obj.discount_status = 1;
      return await discountRule.findAll({
        where: obj
      });
    });
  };

  exports.checkDiscountCoupon = async (data, author) => { 
    return await owner.getOwnerId(author).then(async owner_id => {
       return await discountRule.findOne({
            where : {
                owner_id: owner_id,
                discount_code : data.discount_code, 
            }
       }).then( async (data) =>{
          console.log("==>", data.end_date);
            if(data.end_date){
                if(dateCheck(data.start_date,data.end_date, new Date())){
                    console.log("Availed");
                    return  data; 
                } else {
                    return  "Not Available";
                }
            }    
       });
    });
  };

  function dateCheck(from,to,check) {
            
    var fDate,lDate,cDate;
    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);

    if((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    return false;
}