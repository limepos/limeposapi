var itemGroup = require("../models/itemGroupModel");
var item = require("../models/productGroupItem");

var fs = require("fs");
var json2xls = require("json2xls");
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const config = require("../config/emailConfig.js");

exports.addNewItemGroup = async (itemGroupdata, author) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await itemGroup
      .findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where: {
          owner_id: owner_id
        }
      })
      .then(async data => {
        let count = JSON.stringify(data); 
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
        if (pp[0].count) {
          dd = pp[0].count;
          num = parseInt(dd) + 1;
        } else {
          dd = 1;
          num = parseInt(dd);
        }
        const variatId = num.toString().padStart(5, "0");
        itemGroupdata.item_group_id = owner_id + "" + variatId;
        itemGroupdata.ig_status = 1;
        itemGroupdata.owner_id = owner_id;
        itemGroupdata.sequelnumber = num;
        return await itemGroup.create(itemGroupdata).then(itemdata => {
          var ids = itemGroupdata.product_ids.split(",");
          var i;
          let gItem = {};
          gItem.owner_id = itemdata.owner_id;
          gItem.item_group_id = itemdata.item_group_id;
          gItem.item_status = 1;
          for (i = 0; i < ids.length; i++) {
            gItem.groups_item_id = Date.now() + "-" + ids[i];
            gItem.product_id = ids[i];
            item.create(gItem);
          }
        });
      });
  });
};

exports.getItemGroupById = async (author, params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    itemGroup.hasMany(item, { foreignKey: "item_group_id" });
    return await itemGroup.findOne({
      where: {
        owner_id: owner_id,
        item_group_id: params.itemId,
        ig_status: 1
      },
      include: [{ model: item, attributes: ["product_id"] }]
    });
  });
};

exports.getAllItemGroup = async author => {
  return await owner.getOwnerId(author).then(async owner_id => {
    itemGroup.hasMany(item, { foreignKey: "item_group_id" });
    return await itemGroup.findAll({
      where: {
        owner_id: owner_id,
        ig_status: 1
      },
      include: [{ model: item, attributes: ["product_id"] }]
    });
  });
};

exports.updateItemsGroupById = async (data, author, params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await itemGroup
      .update(data, {
        where: {
          owner_id: owner_id,
          item_group_id: params.itemId,
          ig_status: 1
        }
      })
      .then(() => {
        item
          .destroy({
            where: {
              item_group_id: params.itemId
            }
          })
          .then(() => {
            console.log("variantIds", data);
            var ids = data.product_ids.split(",");
            console.log(ids);
            var i;
            let gItem = {};
            gItem.owner_id = owner_id;
            gItem.item_group_id = params.itemId;
            gItem.item_status = 1;
            for (i = 0; i < ids.length; i++) {
              gItem.groups_item_id = Date.now() + "-" + ids[i];
              gItem.product_id = ids[i];

              item.create(gItem);
            }
          });
      });
  });
};

exports.deleteItemGroupById = async (author, params) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await itemGroup.update(
      { ig_status: 0 },
      {
        where: {
          owner_id: owner_id,
          item_group_id: params.itemId,
          ig_status: 1
        }
      }
    );
  });
};

exports.deleteAllItemGroup = async (data, author) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    let ids = data.ids.split(",");

    return await itemGroup.update(
      { ig_status: 0 },
      {
        where: {
          item_group_id: ids,
          owner_id: owner_id
        }
      }
    );
  });
};

exports.itemGroupFilter = async (author, query) => {
  var obj = JSON.parse(query);

  return await owner.getOwnerId(author).then(async owner_id => {
    obj.owner_id = owner_id;
    obj.ig_status = 1;
    return await itemGroup.findAll({
      where: obj
    });
  });
};

exports.itemGroupImport = async (addondata, author) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    console.log("json ==> ", addondata.data);

    let data = JSON.parse(addondata.data);

    for (var key in data.data) {
      await itemGroup
        .findAll({
          attributes: [
            [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
          ],
          where: {
            owner_id: owner_id
          }
        })
        .then(async id => {
          var count = JSON.stringify(id);
          var pp = JSON.parse(count);

          var num;
          if (pp[0].count) {
            dd = pp[0].count;
            num = parseInt(dd) + 1;
          } else {
            dd = 1;
            num = parseInt(dd);
          }
          var addonId = num.toString().padStart(5, "0");
          data.data[key].item_group_id = owner_id + "" + addonId; //Combination of  Owner id and Generated category id
          data.data[key].ig_status = 1;
          data.data[key].owner_id = owner_id;
          data.data[key].sequelnumber = num;
          await itemGroup.create(data.data[key]).then(itemGroups => {
            var ids = data.data[key].product_ids.split(",");
            var i;
            let grupItem = {};
            grupItem.owner_id = itemGroups.owner_id;
            grupItem.item_group_id = itemGroups.item_group_id;
            grupItem.item_status = 1;
            for (i = 0; i < ids.length; i++) {
              grupItem.groups_item_id = Date.now() + "-" + ids[i];
              grupItem.product_id = ids[i];

              item.create(grupItem);
            }
          });
        });
    }
  });
};

exports.itemGroupExport = async (userdata, author) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    itemGroup.hasMany(item, { foreignKey: "item_group_id" });
    return await itemGroup
      .findAll({
        where: {
          owner_id: owner_id,
          ig_status: 1
        },
        include: {
          model: item,
          attributes: ["product_id"]
        }
      })
      .then(async data => {
        var count = JSON.stringify(data);
        var p = JSON.parse(count);
        p.forEach(e => {
          var variant_id = [];
          e.res_product_group_items.forEach(id => {
            console.log(">>>", id.product_id);
            variant_id.push(id.product_id);
          });
          e.product_ids = variant_id;
        });

        var xls = json2xls(p, {
          fields: ["item_group_name", "number_of_items", "product_ids"]
        });

        const transporter = config.transporter();

        const mailOptions = {
          from: config.fromEmail(),
          to: userdata.email,
          subject: "Lime POS Addon Group Data",
          text: "Hi please find attachment for Item Group export data.",
          attachments: [
            {
              // utf-8 string as an attachment
              filename: "itemGroup" + Date.now() + ".xlsx",
              content: new Buffer(xls, "binary")
            }
          ]
        };

        transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.log(error);
          } else {
            return "Item Group Export Sucessfull";
          }
        });
      });
  });
};
