var User = require("../models/userModel");
var Shop = require("../models/shopModel");
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");

exports.createUser = async userdata => {
  //console.log("userdata ==> ", userdata)
  await User.findAll({
    attributes: [[Sequelize.fn("MAX", Sequelize.col("owner_id")), "count"]]
  }).then(async data => {
    let count = JSON.stringify(data);
    let p = JSON.parse(count);
    let s = 0;
    let num; 
    if(p[0].count){
       s = p[0].count;
       num = parseInt(s) + 1;
     }else {
      s =  1;
       num = parseInt(s);
    }
   
    let ownerId = num.toString().padStart(4, "0");
    userdata.owner_id = ownerId;
    userdata.status = 1;

    await User.create(userdata).then(async user => {
      await Shop.findAll({
        attributes: [[Sequelize.fn("MAX", Sequelize.col("sequelnumber")),"count"]],
        where : {
          owner_id : ownerId
         }
      }).then(async data => {
         let countshop = JSON.stringify(data);
         let pp = JSON.parse(countshop);
         //console.log("shop_count", pp[0].count);
         let dd = 0;
         let shopnum;
           if(pp[0].count){
               dd = pp[0].count;
              
              shopnum = parseInt(dd) + 1;
            }else {
             dd =  1;
              shopnum = parseInt(dd);
           }
        
         userdata.owner_id = ownerId;
         let ShopId = ownerId + "" + shopnum.toString().padStart(4, "0");
       // console.log("countshop  ==== > ", data);

        await Shop.create({
          shop_id: ShopId,
          owner_id: ownerId,
          sequelnumber: dd,
          shop_status: 1
        }).then(data => {
          return user;
        });
      }); 
    });
  });
};
exports.getAllUsers = async author => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await User.findAll();
  });
};
exports.getUser = async obj => {
  return await User.findOne({
    where: obj
  });
};

exports.getProfile = async author => {
  return await owner.getOwnerId(author).then(async owner_id => {
    console.log("ownerID == > ", owner_id);

    return await User.findOne({
      where: {
        owner_id: owner_id
      }
    });
  });
};

exports.getCount = async () => {
  console.log("here");
  return await User.findAll({
    attributes: [[Sequelize.fn("COUNT", Sequelize.col("owner_id")), "count"]]
  });
};

exports.updateUser = async (userdata, params) => {
  const owner = await User.update(userdata, {
    where: { owner_id: params.ownerid }
  });
  if (owner) {
    return await User.findOne({ where: { owner_id: params.ownerid } });
  }
};

exports.updateProfile = async (userdata, author) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    const owner = await User.update(userdata, {
      where: { owner_id: owner_id }
    });
    if (owner) {
      return await User.findOne({ where: { owner_id: owner_id } });
    }
  });
};

exports.passwordUpdate = async (userdata, author) => {
  return await owner.getOwnerId(author).then(async owner_id => {
    return await User.findOne({ where: { owner_id: owner_id } }).then(
      async user => {
        console.log("userData == >", user.owner_id);

        let newpassword = bcrypt.hashSync(userdata.newpassword, 10);

        if (bcrypt.compareSync(userdata.password, user.password)) {
          return await User.update(
            { password: newpassword },
            { where: { owner_id: owner_id } }
          );
        } else {
          return "Please Enter Correct Password";
        }
      }
    );
  });
};

exports.forgotPasswordSendOtp = async userdata => {
  var val = Math.floor(1000 + Math.random() * 9000);
  console.log(val);
  await User.update({ token: val }, { where: { email: userdata.email } }).then(
    data => {
      const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "lakhan.appson1991@gmail.com",
          pass: "appson@123" // naturally, replace both with your real credentials or an application-specific password
        }
      });

      const mailOptions = {
        from: "no-replay@appsontechnologies.in",
        to: userdata.email,
        subject: "Lime POS Forgot Password",
        text:
          "Hi you OTP is " +
          val +
          " Please user this four digit code to change your password"
      };

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log("Email sent: " + info.response);
        }
      });
    }
  );
};

exports.checkotp = async userdata => {
  return await User.findOne({
    where: { owner_id: userdata.owner_id, token: userdata.otp }
  });
};

exports.newPassword = async userdata => {
  User.findOne({
    where: { owner_id: userdata.owner_id, token: userdata.otp }
  }).then(async user => {
    const newhaspassword = bcrypt.hashSync(userdata.newpassword, 10);
    console.log(newhaspassword);
    return await User.update(
      { password: newhaspassword, token: "" },
      { where: { owner_id: userdata.owner_id } }
    );
  });
};

exports.getUsers = async params => {
  return await User.findOne({ where: { owner_id: params.ownerid } });
};

exports.deleteUser = async params => {
  return await User.update(
    { statu: -1 },
    { where: { owner_id: params.ownerid } }
  );
};

exports.disableUser = async params => {
  return await User.update(
    { statu: 0 },
    { where: { owner_id: params.ownerid } }
  );
};

exports.enableUser = async params => {
  return await User.update(
    { statu: 1 },
    { where: { owner_id: params.ownerid } }
  );
};
