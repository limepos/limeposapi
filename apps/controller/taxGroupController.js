var taxGroup  = require('../models/taxGroupModel');
var taxGroupItem  = require('../models/taxGroupItemModel');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");

exports.addNewTaxGroup = async (itemGroupdata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await taxGroup
        .findAll({
          attributes: [
            [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
          ],
          where: {
            owner_id: owner_id
          }
        })
        .then(async data => {
          let count = JSON.stringify(data); 
          let pp = JSON.parse(count);
          let dd = 0;
          let num;
          if (pp[0].count) {
            dd = pp[0].count;
            num = parseInt(dd) + 1;
          } else {
            dd = 1;
            num = parseInt(dd);
          }
          const taxgroupId = num.toString().padStart(5, "0");
          itemGroupdata.tax_group_id = owner_id + "" + taxgroupId;
          itemGroupdata.txg_status = 1;
          itemGroupdata.owner_id = owner_id;
          itemGroupdata.sequelnumber = num;
          return await taxGroup.create(itemGroupdata).then(itemdata => {
            var ids = itemGroupdata.tax_ids.split(",");
            var i;
            let gItem = {};
            gItem.owner_id = itemdata.owner_id;
            gItem.tax_group_id = itemdata.tax_group_id;
            gItem.item_status = 1;
            for (i = 0; i < ids.length; i++) {
              gItem.tax_groups_items_id = Date.now() + "-" + ids[i];
              gItem.tax_id = ids[i];
              taxGroupItem.create(gItem);
            }
          });
        });
    });
  };

  exports.getTaxGroupById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        taxGroup.hasMany(taxGroupItem, { foreignKey: "tax_group_id" });
      return await taxGroup.findOne({
        where: {
          owner_id: owner_id,
          tax_group_id: params.taxId,
          txg_status: 1
        },
        include: [{ model: taxGroupItem, attributes: ["tax_id"] }]
      });
    });
  };

  exports.updateTaxGroupById = async (data, author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await taxGroup
        .update(data, {
          where: {
            owner_id: owner_id,
            tax_group_id: params.taxId,
            txg_status: 1
          }
        })
        .then(() => {
            taxGroupItem
            .destroy({
              where: {
                tax_group_id: params.taxId
              }
            })
            .then(() => {
              var ids = data.tax_ids.split(",");
              var i;
              let gItem = {};
              gItem.owner_id = owner_id;
              gItem.tax_group_id = params.taxId;
              gItem.item_status = 1;
              for (i = 0; i < ids.length; i++) {
                gItem.tax_groups_items_id = Date.now() + "-" + ids[i];
                gItem.tax_id = ids[i];
                taxGroupItem.create(gItem);
              }
            });
        });
    });
  };

  exports.getAllTaxesGroup = async author => {
    return await owner.getOwnerId(author).then(async owner_id => {
     taxGroup.hasMany(taxGroupItem, { foreignKey: "tax_group_id" });
      return await taxGroup.findAll({
        where: {
          owner_id: owner_id,
          txg_status: 1
        },
        include: [{ model: taxGroupItem, attributes: ["tax_id"] }]
      });
    });
  };

  exports.taxGroupDeletById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await taxGroup.update(
        { txg_status: 0 },
        {
          where: {
            owner_id: owner_id,
            tax_group_id: params.taxId,
            txg_status: 1
          }
        }
      );
    });
  };

  exports.allTaxGroupDelete = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
     
      let ids = data.ids.split(",");
      return await taxGroup.update(
        { txg_status: 0 },
        {
          where: {
            owner_id: owner_id,
            tax_group_id: ids
          }
        }
      );
    });
  };
  
  exports.taxGroupFilter = async (author, query) => {
    var obj = JSON.parse(query);
    return await owner.getOwnerId(author).then(async owner_id => {
      obj.owner_id = owner_id;
      obj.txg_status = 1;
      return await taxGroup.findAll({
        where: obj
      });
    });
  };
  