var VariantGroup  = require('../models/variantGroupModel'); 
var variantGroupItem  = require('../models/variantsGroupsItem');
var fs = require('fs');
var json2xls = require('json2xls');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const config = require("../config/emailConfig.js");

exports.addNewVariantGroup = async (varinatdata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
      return await VariantGroup.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const variatId = num.toString().padStart(5, "0");
        varinatdata.variant_group_id = owner_id + "" + variatId;   //Combination of  Owner id and Generated category id 
        varinatdata.vg_status = 1;
        varinatdata.owner_id = owner_id;
        varinatdata.sequelnumber = num;
        return await VariantGroup.create(varinatdata).then((variantGroup)=>{
             
            console.log("variantIds", variantGroup.owner_id);
             
            var ids = varinatdata.variant_ids.split(",");

            console.log(ids);

            var i;
            let varinatItem = { };
            varinatItem.owner_id =  variantGroup.owner_id;
            varinatItem.variant_group_id = variantGroup.variant_group_id;
            varinatItem.item_status = 1;
            for (i = 0; i < ids.length; i++) {
              varinatItem.variants_groups_item_id = Date.now()+'-'+ids[i];
              varinatItem.variant_id = ids[i];
             // console.log("varinatItem", varinatItem);
                variantGroupItem.create(varinatItem);
            }

        });
      });
    });
  };

   exports.getVariantGroups = async (author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
         VariantGroup.hasMany(variantGroupItem, {foreignKey: 'variant_group_id'});
        // variantGroupItem.belongsTo(VariantGroup, {foreignKey: 'variant_group_id', attributes: ['variant_id']});
         return await VariantGroup.findAll({ where : {
            owner_id: owner_id,
            vg_status : 1
        },
        order: [
             ['vg_sort_order', 'ASC']
          ],
           include: [
              { model: variantGroupItem, attributes: ['variant_id'] }
          ]
       }
       );
    }); 
  };

  
  exports.getVariantGroupbyId = async (author,params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      VariantGroup.hasMany(variantGroupItem, {foreignKey: 'variant_group_id'});
      variantGroupItem.belongsTo(VariantGroup, {foreignKey: 'variant_group_id'});
        return await VariantGroup.findOne({ where : {
            owner_id: owner_id,
            variant_group_id:  params.groupId,
            vg_status : 1
        },
        include: [variantGroupItem]
       });
    }); 
  };

  exports.updateVariantGroupbyId = async (data, author,params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await VariantGroup.update(data, { where: {
            owner_id: owner_id,
            variant_group_id:  params.groupId
        }
       }).then((variantGroup)=>{

         console.log("==>", data); 
         console.log("==>", params.groupId);
         
      //   let variant = variantGroup;
         
        variantGroupItem.destroy({ where : {
            variant_group_id:  params.groupId
        }
       }).then(() => {
        console.log("variantIds", data );
        var ids = data.variant_ids.split(",");
        console.log(ids);
        var i;
        let varinatItem = { };
        varinatItem.owner_id =  owner_id;
        varinatItem.variant_group_id = params.groupId;
        varinatItem.item_status = 1;
        for (i = 0; i < ids.length; i++) {
          varinatItem.variants_groups_item_id = Date.now()+'-'+ids[i];
          varinatItem.variant_id = ids[i];
         // console.log("varinatItem", varinatItem);
            variantGroupItem.create(varinatItem);
        }
      });
        
      });
    }); 
  };

  exports.deleteVariantGroupbyId = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await VariantGroup.update({
            vg_status : 0
        }, { where: {
            owner_id: owner_id,
            variant_group_id:  params.groupId
        }
       });
    });
  };


  
exports.deleteAllVariantGroup = async (data, author, params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
      console.log(data);
      let ids = data.ids.split(",");
       // let ids = ['00010001', '00010002'];
         return await  VariantGroup.update({ vg_status : 0 },{ where : { variant_group_id : ids, owner_id:owner_id }} );
        });
      
    };
  
    exports.variantGroupFilter = async (author, query) => {
      var obj = JSON.parse(query);
     // console.log("filter ==> ", obj);
      return await owner.getOwnerId(author).then(async owner_id => {
          obj.owner_id = owner_id;
          obj.vg_status = 1;
            return await VariantGroup.findAll({
              where: obj
            });
      });
    };

    exports.variantGroupImport = async (varinatdata, author) => {

        return await owner.getOwnerId(author).then(async owner_id => {
      
       console.log("json ==> ", varinatdata.data);

       let data = JSON.parse(varinatdata.data);
     
    
        for ( var key in data.data) {
          await VariantGroup.findAll({
            attributes: [
              [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
            ],
            where : {
              owner_id : owner_id
             }
          }).then(async (id) => {
            var count = JSON.stringify(id);
            var pp = JSON.parse(count);
          
          var num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
            var variatId = num.toString().padStart(5, "0");
            data.data[key].variant_group_id = owner_id + "" + variatId;   //Combination of  Owner id and Generated category id 
            data.data[key].vg_status = 1;
            data.data[key].owner_id = owner_id;
            data.data[key].sequelnumber = num;
             await VariantGroup.create(data.data[key]).then((variantGroup)=>{
             
              console.log("variantIds", variantGroup.owner_id);
              var ids = data.data[key].variant_ids.split(",");
              console.log(ids);
              var i;
              let varinatItem = { };
              varinatItem.owner_id =  variantGroup.owner_id;
              varinatItem.variant_group_id = variantGroup.variant_group_id;
              varinatItem.item_status = 1;
              for (i = 0; i < ids.length; i++) {
                varinatItem.variants_groups_item_id = Date.now()+'-'+ids[i];
                varinatItem.variant_id = ids[i];
               // console.log("varinatItem", varinatItem);
                  variantGroupItem.create(varinatItem);
              }
  
          });
          });
         }
        });
      };
    
        
      exports.variantGroupExport = async (userdata, author) => {
    
       //   console.log("jhasdkjf",userdata.email);
    
        return await owner.getOwnerId(author).then(async owner_id => {

          VariantGroup.hasMany(variantGroupItem, {foreignKey: 'variant_group_id'});
         // variantGroupItem.belongsTo(VariantGroup, {foreignKey: 'variant_group_id'});

        //   console.log("variantGroupItem ----------------->", variantGroupItem ); 

          return await VariantGroup.findAll({ where:{
            owner_id: owner_id,
            vg_status : 1
             },
             include: {
              model: variantGroupItem,
              attributes: ['variant_id']
            }
          }).then( async data => {
    
            var count = JSON.stringify(data);
            var p = JSON.parse(count);
                
          
              p.forEach(e => {
                  // console.log(">>>", e);
              
                   var  variant_id = [];
                  

                   e.res_product_variants_groups_items.forEach(id => {
                    console.log(">>>", id.variant_id);
                    variant_id.push(id.variant_id);
                });
                e.variant_ids = variant_id;
              });
                 console.log(">>>", p);

             // array1.forEach(element => console.log(element));
              
           //  console.log("variants", p);

             var xls = json2xls(p,{
              fields: ['variant_group_name', 'vg_sort_order', 'variant_ids']
          });
             //console.log(xls);
            // fs.writeFileSync('exportsdata/variant.xlsx', xls, 'binary');
    
             const transporter = config.transporter();
      
            const mailOptions = {
              from: config.fromEmail(),
              to: userdata.email,
              subject: "Lime POS Variant Group Data",
              text:
                "Hi please find attachment for variant group export data.",
                attachments: [
                  {   // utf-8 string as an attachment
                      filename: 'variantGroup'+Date.now()+'.xlsx',
                      content: new Buffer( xls, 'binary')
                  }
                ]
            };
      
            transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log(error);
              } else {
                return "Varint Group Export Sucessfull";
              }
            });
    
          });
        });
      };