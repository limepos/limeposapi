var product  = require('../models/productModel'); 
var fs = require("fs");
var json2xls = require("json2xls");
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const nodemailer = require("nodemailer");
const config = require("../config/emailConfig.js");

exports.addNewProduct = async (productdata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
      return await product.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const productId = num.toString().padStart(5, "0");
        productdata.product_id = owner_id + "" + productId;   //Combination of  Owner id and Generated Product id 
        productdata.product_status = 1;
        productdata.owner_id = owner_id;
        productdata.sequelnumber = num;
        return await product.create(productdata);
      });
    });
  };

  exports.getAllProducts = async (author) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await product.findAll(
            { 
             where: {
              owner_id: owner_id,
              product_status : 1
              },
              order: [
                ['p_sort_order', 'ASC']
              ],
           },
        
        );
     
    });
  };

  exports.getProductById = async (author,params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await product.findOne(
            { 
             where: {
              owner_id: owner_id,
              product_id : params.proId,
              product_status : 1
              }
            }
        );
    });
  };

  exports.productUpdateById = async (prodata, author, params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await product.update(prodata,
            { 
             where: {
              owner_id: owner_id,
              product_id : params.proId
              
              }
            }
        );
    });
  };

  exports.productDeleteById = async (author, params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await product.update({product_status : 0 },
            { 
             where: {
              owner_id: owner_id,
              product_id : params.proId
              
              }
            }
        );
    });
  };
  
  
  exports.productDeleteAll = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
    console.log(data);
    let ids = data.ids.split(",");
     // let ids = ['00010001', '00010002'];
       return await  product.update({ product_status : 0 },{ where : { product_id : ids, owner_id:owner_id }} );
      });
    
  };
  

  exports.productFilter = async (author, query) => {
    var obj = JSON.parse(query);
    console.log("filter == > ", obj);
    return await owner.getOwnerId(author).then(async owner_id => {
        obj.owner_id = owner_id;
        obj.product_status = 1;
          return await product.findAll({
            where: obj
          });
    });
  };
  
  exports.productImport = async (prodata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      console.log("json ==> ", prodata.data);
  
      let data = JSON.parse(prodata.data);
  
      for (var key in data.data) {
        await product
          .findAll({
            attributes: [
              [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
            ],
            where: {
              owner_id: owner_id
            }
          })
          .then(async id => {
            var count = JSON.stringify(id);
            var pp = JSON.parse(count);
  
            var num;
            if (pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            } else {
              dd = 1;
              num = parseInt(dd);
            }
            var productId = num.toString().padStart(5, "0");
            data.data[key].product_id = owner_id + "" + productId; //Combination of  Owner id and Generated category id
            data.data[key].product_status = 1;
            data.data[key].owner_id = owner_id;
            data.data[key].sequelnumber = num;
            await product.create(data.data[key]);
          });
      }
    });
  };
  
  exports.productExport = async (userdata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await product
        .findAll({
          where: {
            owner_id: owner_id,
            product_status: 1
          }
        })
        .then(async data => {
          var count = JSON.stringify(data);
          var p = JSON.parse(count);
          var xls = json2xls(p, { 
               fields: ["product_name", "shop_id", "category_id", "tax_group_id","product_price","variant_group_id","addon_group_id","product_type","registers_id","unit","note","p_sort_order"]
           });
  
          const transporter = config.transporter();
  
          const mailOptions = {
            from: config.fromEmail(),
            to: userdata.email,
            subject: "Lime POS Product Data",
            text: "Hi please find attachment for Product export data.",
            attachments: [
              {
                // utf-8 string as an attachment
                filename: "product" + Date.now() + ".xlsx",
                content: new Buffer(xls, "binary")
              }
            ]
          };
  
          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              console.log(error);
            } else {
              return "Product has Exported Sucessful";
            }
          });
        });
    });
  };

  exports.productQueckAdd = async (prodata, author) => {
    
    return await owner.getOwnerId(author).then(async owner_id => {
      console.log("json ==> ", prodata.data);
  
      let data = JSON.parse(prodata.data);
      console.log(data);

      console.log(data.data[0].keys.length);
  
       for (var i = 0 ; i < data.data[0].keys.length; i++) {
          console.log("==>",i);
         await product
         .findAll({
           attributes: [
              [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
            ],
            where: {
              owner_id: owner_id
            }
          })
          .then(async id => {
            var count = JSON.stringify(id);
            var pp = JSON.parse(count);
  
            var num;
            if (pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            } else {
              dd = 1;
              num = parseInt(dd);
            }
            var productId = num.toString().padStart(5, "0");
             var p = {};
            p.product_id = owner_id + "" + productId; //Combination of  Owner id and Generated category id
            p.product_status = 1;
            p.owner_id = owner_id;
            p.sequelnumber = num;
            p.product_name =  data.data[0].productname[i];
            p.category_id = data.data[0].category[i];
            p.tax_group_id = data.data[0].taxgroup[i];
            p.product_price =  data.data[0].price[i];

            console.log(p);
            await product.create(p);
         });
       }
    });

   };
  