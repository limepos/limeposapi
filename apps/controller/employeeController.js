var employee  = require('../models/employeeModel');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");

exports.addNewEmployee = async (emplydata, author) => {

    return await owner.getOwnerId(author).then(async owner_id => { 

        return await employee.findAll({
            attributes: [
              [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
            ],
            where : {
              owner_id : owner_id
             }
          }).then(async data => {
            let count = JSON.stringify(data);
            let pp = JSON.parse(count);
            let dd = 0;
            let num;
              if(pp[0].count) {
                  dd = pp[0].count;
                  num = parseInt(dd) + 1;
                
               } else {
                 dd =  1;
                 num = parseInt(dd);
              }
            const employeeId = num.toString().padStart(5, "0");
            emplydata.employee_id = owner_id + "" + employeeId;   //Combination of  Owner id and Generated Product id 
            emplydata.employee_status = 1;
            emplydata.owner_id = owner_id;
            emplydata.sequelnumber = num;
            return await employee.create(emplydata);
          });

    });
};
exports.getEmployeeById = async (author,params) => {

    return await owner.getOwnerId(author).then(async owner_id => {
    
        return await employee.findOne(
            { 
             where: {
              owner_id: owner_id,
              employee_id : params.empId,
              employee_status : 1
              }
            }
        );
    });
  };

  exports.updateEmployeeById = async (data, author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await employee.update(data,
            { 
             where: {
              owner_id: owner_id,
              employee_id : params.empId
              }
            }
        );
    });
  }; 

  
  exports.deleteEmployeeById = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await employee.update({ employee_status : 0 },
            { 
             where: {
              owner_id: owner_id,
              employee_id : params.empId
              }
            }
        );
    });
  }; 

  exports.getEmployeeByRole = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        return await employee.findAll({ 
             where: {
                owner_id: owner_id,
                employee_status : 1,
                role_id : params.roleId
              }
           }
        );
    });
  }; 
  