var otg  = require('../models/orderTicketGroupModel');
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
exports.addNewOrderTicket = async (otgdata, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await otg.findAll({
        attributes: [
          [Sequelize.fn("MAX", Sequelize.col("sequelnumber")), "count"]
        ],
        where : {
          owner_id : owner_id
         }
      }).then(async data => {
        let count = JSON.stringify(data);
        let pp = JSON.parse(count);
        let dd = 0;
        let num;
          if(pp[0].count) {
              dd = pp[0].count;
              num = parseInt(dd) + 1;
            
           } else {
             dd =  1;
             num = parseInt(dd);
          }
        const registerId = num.toString().padStart(4, "0");
        otgdata.ticket_group_id = owner_id + "" + registerId;
        otgdata.ticket_group_status = 1;
        otgdata.owner_id = owner_id;
        otgdata.sequelnumber = num;
        return await otg.create(otgdata);
      });
    });
  };

  exports.allOrderTickets = async (author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await otg.findAll({
        where: {
          owner_id: owner_id,
          ticket_group_status : 1
        }
      });
    });
  };

  exports.orderTicket = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await otg.findOne({
        where: {
          owner_id: owner_id,
          ticket_group_status : 1,
          ticket_group_id : params.orderId
        }
      });
    });
  };
  
  exports.orderTicketDelete = async (author, params) => {
    return await owner.getOwnerId(author).then(async owner_id => {
      return await otg.update({ ticket_group_status : 0 },{
        where: {
          owner_id: owner_id,
          ticket_group_id : params.orderId
        }
      });
    });
  };

  exports.orderTicketDeleteAll = async (data, author) => {
    return await owner.getOwnerId(author).then(async owner_id => {
        console.log(data);
        let ids = data.ids.split(",");
         // let ids = ['00010001', '00010002'];
           return await  otg.update({ ticket_group_status : 0 },{ where : { ticket_group_id : ids,  owner_id: owner_id }} );
          });
        
  };
  
  
  