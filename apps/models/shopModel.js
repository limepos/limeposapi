const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const Shop = sequelize.define('res_shops', {
    shop_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    shop_logo: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    shop_name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    business_type: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    city: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    owner_pin: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    shop_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
Shop.sync()
.then()
.catch();

module.exports = Shop;