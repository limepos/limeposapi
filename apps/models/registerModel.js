// create user model
const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const Register = sequelize.define('res_registers', {
    registers_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    register_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    receipt_prefix: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    bill_header: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    bill_footer: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    number_of_tables: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    register_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
Register.sync()
.then()
.catch();

module.exports = Register;