const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const employees = sequelize.define('res_employees', {
    employee_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    employee_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    pin: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    registers_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    manager_permissions: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },
    role_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    employee_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
employees.sync()
.then()
.catch();

module.exports = employees;