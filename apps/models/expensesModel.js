const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const expenses = sequelize.define('res_expenses', {
    expenses_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    registers_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    expanse_amount: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    employee_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    expenses_note: {
        type: Sequelize.STRING,
        allowNull: true
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    expanses_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
expenses.sync()
.then()
.catch();

module.exports = expenses;