const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const Category = sequelize.define('res_product_categories', {
    category_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    ticket_group_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    category_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    cat_sort_order: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    category_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
Category.sync()
.then()
.catch();

module.exports = Category;