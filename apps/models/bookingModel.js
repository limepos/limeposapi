const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const customer = sequelize.define('res_booking', {
    booking_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    registers_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    shipping_address: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    city: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    state: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    pincode: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    booking_date:{
        type: Sequelize.STRING,
        allowNull: false
    },
    delivery_date:{
        type: Sequelize.STRING,
        allowNull: false
    },
    delivery_time:{
        type: Sequelize.STRING,
        allowNull: false
    },
    door_delivery:{
        type: Sequelize.INTEGER,
        allowNull: true
    },
    booking_note:{
        type: Sequelize.STRING,
        allowNull: true
    },
    booking_advance_payment:{
        type: Sequelize.STRING,
        allowNull: true
    },
    contact_number:{
        type: Sequelize.STRING,
        allowNull: false
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    booking_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
customer.sync()
.then()
.catch();

module.exports = customer;