const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const addonGroupItem = sequelize.define('res_product_addon_group_item', {
   addon_groups_item_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    addon_group_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    addon_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_status:{
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
addonGroupItem.sync()
.then()
.catch();
module.exports = addonGroupItem;