const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const shiftChange = sequelize.define('res_shiftchange', {
    shift_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    registers_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    cashier_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    last_amount: {
        type: Sequelize.STRING,
        allowNull: false,
    }
});
// create table with user model
shiftChange.sync()
.then()
.catch();

module.exports = shiftChange;