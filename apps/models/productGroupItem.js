const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const productGroupItem = sequelize.define('res_product_group_item', {
    groups_item_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_group_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    product_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_status:{
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
productGroupItem.sync()
.then()
.catch();
module.exports = productGroupItem;