const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const Variant = sequelize.define('res_product_variants', {
    variant_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_comment: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_price: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_sort_order: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    linked: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    variant_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
Variant.sync()
.then()
.catch();

module.exports = Variant;