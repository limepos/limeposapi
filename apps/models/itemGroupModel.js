const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const itemGroup = sequelize.define('res_product_item_groups', {
    item_group_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_group_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    number_of_items:{ 
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    ig_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
itemGroup.sync()
.then()
.catch();

module.exports = itemGroup;