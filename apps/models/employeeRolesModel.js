const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const employeeRole = sequelize.define('res_employees_roles', {
    role_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    role_name:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    role_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
employeeRole.sync()
.then()
.catch();

module.exports = employeeRole;