const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const tax = sequelize.define('res_product_taxes', {
    tax_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    tax_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    tax_percent: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    tax_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
tax.sync()
.then()
.catch();
module.exports = tax;