const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const customer = sequelize.define('res_customer', {
    customer_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    mobile_no: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    first_name: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    last_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    order_count: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    last_order:{
        type: Sequelize.STRING,
        allowNull: true
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    customer_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
customer.sync()
.then()
.catch();
module.exports = customer;