const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const variantGroupItem = sequelize.define('res_product_variants_groups_item', {
    variants_groups_item_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_group_id : {
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
variantGroupItem.sync()
.then()
.catch();
module.exports = variantGroupItem;