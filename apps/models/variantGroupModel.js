const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const VariantGroup = sequelize.define('res_product_variants_groups', {
    variant_group_id : {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    variant_group_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    vg_sort_order: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    vg_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
VariantGroup.sync()
.then()
.catch();

module.exports = VariantGroup;