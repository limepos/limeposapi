const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const Addon = sequelize.define('res_product_addons', {
    addon_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    addon_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    addon_price: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    addon_sort_order: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    addon_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
Addon.sync()
.then()
.catch();

module.exports = Addon;