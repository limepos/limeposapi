const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const order = sequelize.define('res_order', {
    order_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    registers_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    order_items: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    order_total: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    order_subtotal: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    order_discount: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    ticket_group_id:{
        type: Sequelize.STRING,
        allowNull: true
    },
    payment_by: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    refunded: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    refund_by: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    cancellation_notes: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    order_node: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    waiter_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    cashier_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ordert_type: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    booking_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    customer_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    table_name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    occupants: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    discount_rule_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    order_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
order.sync()
.then()
.catch();

module.exports = order;