const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const OrderTicketGroup = sequelize.define('res_order_ticket_group', {
    ticket_group_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    ticket_group_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
     ticket_group_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
OrderTicketGroup.sync()
.then()
.catch();

module.exports = OrderTicketGroup;