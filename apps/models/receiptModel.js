const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const receipts = sequelize.define('res_receipts', {
    receipt_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    receipt_number: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    customer_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    receipt_order_status: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    payment_status: {
        type: Sequelize.STRING,
        allowNull: true
    },
    order_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    receipt_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
receipts.sync()
.then()
.catch();
module.exports = receipts;