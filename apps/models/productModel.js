const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const products = sequelize.define('res_products', {
    product_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: true,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    product_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    category_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    tax_group_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    product_price: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    variant_group_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    addon_group_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    product_type: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    registers_id: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    unit: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    note: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    p_sort_order: {
        type: Sequelize.INTEGER,
        allowNull: true,
    }, 
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    product_status: {
        type: Sequelize.INTEGER,
        allowNull: false, 
        defaultValue: 0,
    }
});
// create table with user model
products.sync()
.then()
.catch();

module.exports = products;