const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const addonGroup = sequelize.define('res_product_addon_groups', {
    addon_group_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    addon_group_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    min_selectable: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    max_selectable: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    adg_sort_order: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    adg_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
addonGroup.sync()
    .then()
    .catch();

module.exports = addonGroup;