const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const taxGroupItem = sequelize.define('res_product_tax_groups_items', {
    tax_groups_items_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    tax_group_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    tax_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
taxGroupItem.sync()
.then()
.catch();

module.exports = taxGroupItem;