const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const producdiscountRule = sequelize.define('res_discount_rule_items', {
    discount_rules_item_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    discount_rule_id:{
        type: Sequelize.STRING,
        allowNull: false, 
    },
    registers_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    item_status:{
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
producdiscountRule.sync()
.then()
.catch();
module.exports = producdiscountRule;