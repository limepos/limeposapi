const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const discountRules = sequelize.define('res_discount_rules', {
    discount_rule_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    discount_code: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    discount_type: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    discount_on: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    registers_ids: {
        type: Sequelize.STRING,
        allowNull: true
    },
    start_date: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    end_date: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    start_hours: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    end_hours: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    auto_apply: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    discount_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
discountRules.sync()
.then()
.catch();
module.exports = discountRules;