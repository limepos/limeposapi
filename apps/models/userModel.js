// create user model
const Sequelize = require("sequelize");
var sequelize = require("../config/db");
const bcrypt = require("bcrypt");
const User = sequelize.define(
  "res_owners",
  {
    owner_id: {
      type: Sequelize.STRING(20),
      primaryKey: true,
      allowNull: false
    },
    first_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    contact_no: {
      type: Sequelize.STRING,
      allowNull: false
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    token: {
      type: Sequelize.STRING,
      allowNull: true
    },
    status: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  },
  {
    hooks: {
      beforeCreate: User => {
        {
          User.password =
            User.password && User.password != ""
              ? bcrypt.hashSync(User.password, 10)
              : "";
        }
      }
    }
  }
);
// create table with user model
User.sync()
  .then()
  .catch();

// defaultScope: {
//     attributes: { exclude: ['password','token'] },
//   }

module.exports = User;
