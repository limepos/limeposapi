const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const taxGroup = sequelize.define('res_product_tax_groups', {
    tax_group_id: {
        type: Sequelize.STRING(20),
        primaryKey: true,
        allowNull: false,
    },
    shop_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    owner_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    tax_group_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    included: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    sequelnumber:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    txg_status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    }
});
// create table with user model
taxGroup.sync()
.then()
.catch();

module.exports = taxGroup;