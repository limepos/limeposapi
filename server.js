const express = require('express');
const bodyParser = require('body-parser');

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

port = process.env.PORT || 3000;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.listen(port, function () {
    console.log('Express is running on port 3000');
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var routes = require('./apps/routes/appRouter'); //importing route

routes(app); //register the route